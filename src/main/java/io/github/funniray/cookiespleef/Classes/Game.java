package io.github.funniray.cookiespleef.Classes;

import io.github.funniray.cookiespleef.Utils.worlds;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by funniray on 11/22/17.
 */
public class Game {
    private String map;
    private HashMap<Player, InGame> PlayersData;
    private int ID;
    private World world;
    private int state;

    public Game(String map) {
        this.map = map;
        this.PlayersData = new HashMap<>();
        this.ID = Games.Games.size();
        this.world = worlds.loadMap(this.map,this.ID);
        this.state = 0;
        Games.Games.put(this.ID,this);
    }

    public void setWorld(World world){
        this.world = world;
    }

    public String getMap(){
        return map;
    }

    public void removeGame(){
        for (Map.Entry<Player,InGame> data: PlayersData.entrySet()) {
            data.getValue().leave();
            Player player = data.getKey();
            worlds.teleportToWorld(player,"world",8,4.1,8);
            player.getInventory().clear();
            player.setGameMode(GameMode.ADVENTURE);

        }
        Games.Games.remove(ID);
    }

    public void addPlayerData(InGame data){
        PlayersData.put(data.getPlayer(),data);
        Games.Games.replace(this.ID,this);
        Games.PlayerGames.put(data.getPlayer(),this);
    }

    public InGame getPlayerData(Player player){
        return PlayersData.get(player);
    }

    public void setPlayerData(InGame data) {
        PlayersData.replace(data.getPlayer(),data);
        Games.Games.replace(this.ID,this);
    }

    public void removePlayerData(InGame data){
        PlayersData.remove(data);
        Games.Games.replace(this.ID,this);
        Games.PlayerGames.remove(data.getPlayer());
    }

    public void setState(int state){
        this.state = state;
        Games.Games.replace(this.ID,this);
    }

    public int getState(){
        return this.state;
    }

    public void endGame(){
        for (Map.Entry<Player,InGame> data: PlayersData.entrySet()) {
            String subtitle = null;
            if (data.getValue().isLiving()){
                subtitle = ChatColor.DARK_PURPLE+"You win!";
            }
            data.getKey().sendTitle(ChatColor.AQUA+"You killed "+data.getValue().getKills()+" players!",subtitle,5,20,5);
        }
        removeGame();
        new Game(this.map);
    }

    public List<Player> getPlayers(){
        return new ArrayList<>(PlayersData.keySet());
    }

    public World getWorld(){
        return this.world;
    }

    public int getID(){
        return this.ID;
    }

    public void checkLiving(){
        int alive = 0;
        for (Map.Entry<Player,InGame> data: PlayersData.entrySet()) {
            if (data.getValue().isLiving())
                alive = alive + 1;
        }
        if (alive <= 1){
            endGame();
        }
    }
}
