package io.github.funniray.cookiespleef.Classes;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by funniray on 11/22/17.
 */
public class Games {
    static HashMap<Integer,Game> Games = new HashMap<>();
    static HashMap<Player,Game> PlayerGames = new HashMap<>();

    public static Game getGame(String map){
        Game game = null;
        for (Map.Entry<Integer,Game> games:Games.entrySet()){
            if (!games.getValue().getMap().equalsIgnoreCase(map))
                continue;
            game = games.getValue();
        }
        return game;
    }

    public static boolean isPlayerInGame(Player player){
        return PlayerGames.containsKey(player);
    }

    public static Game getPlayersGame(Player player){
        return PlayerGames.get(player);
    }
}
