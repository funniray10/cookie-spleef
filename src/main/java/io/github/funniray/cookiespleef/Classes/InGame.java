package io.github.funniray.cookiespleef.Classes;

import org.bukkit.entity.Player;

/**
 * Created by funniray on 11/22/17.
 */
public class InGame {
    private Player player;
    private Game game;
    private int kills;
    private boolean alive;
    private Player lastHitBy;

    public InGame(Player player, Game game){
        this.player = player;
        this.game = game;
        this.kills = 0;
        this.alive = true;
        this.lastHitBy = this.player;
        game.addPlayerData(this);
    }

    public Player getPlayer(){
        return this.player;
    }

    public void leave(){
        game.removePlayerData(this);
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public InGame setLiving(boolean living) {
        this.alive = living;
        //Makes my life a bit easier
        return this;
    }

    public boolean isLiving(){
        return this.alive;
    }

    public void setLastDamager(Player player){
        this.lastHitBy = player;
    }

    public Player getLastDamager(){
        return this.lastHitBy;
    }
}
