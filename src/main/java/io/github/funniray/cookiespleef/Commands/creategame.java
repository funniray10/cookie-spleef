package io.github.funniray.cookiespleef.Commands;

import io.github.funniray.cookiespleef.Classes.Game;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Created by funniray on 11/23/17.
 */
public class creategame implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0)
            return false;
        Game game = new Game(args[0]);
        sender.sendMessage("You game "+args[0]+" with ID "+game.getID());
        return true;
    }
}
