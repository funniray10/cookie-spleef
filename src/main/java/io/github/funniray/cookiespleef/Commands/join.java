package io.github.funniray.cookiespleef.Commands;

import io.github.funniray.cookiespleef.Classes.Games;
import io.github.funniray.cookiespleef.Classes.InGame;
import io.github.funniray.cookiespleef.Utils.worlds;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by funniray on 6/19/17.
 */
public class join implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0)
            return false;
        if (sender instanceof Player){
            Player player = (Player) sender;
            new InGame(player, Games.getGame(args[0]));
            player.sendMessage("You joined game "+args[0]);
            return true;
        }
        return false;
    }
}
