package io.github.funniray.cookiespleef.Commands;

import io.github.funniray.cookiespleef.Classes.Game;
import io.github.funniray.cookiespleef.Classes.Games;
import io.github.funniray.cookiespleef.Misc.startGame;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by funniray on 11/24/17.
 */
public class stopgame implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Game game = Games.getPlayersGame(player);
            game.endGame();
            player.sendMessage("Game stopped");
            return true;
        }
        return false;
    }
}