package io.github.funniray.cookiespleef;

import io.github.funniray.cookiespleef.Classes.Game;
import io.github.funniray.cookiespleef.Commands.creategame;
import io.github.funniray.cookiespleef.Commands.join;
import io.github.funniray.cookiespleef.Commands.start;
import io.github.funniray.cookiespleef.Commands.stopgame;
import io.github.funniray.cookiespleef.Listeners.*;
import io.github.funniray.cookiespleef.Misc.everyTick;
import io.github.funniray.cookiespleef.Utils.worlds;
import org.bukkit.Bukkit;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public final class Cookiespleef extends JavaPlugin {

    @Override
    public void onEnable() {

        //Registering Events
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new onRightClick(),this);
        pm.registerEvents(new PlayerDamageByPlayer(), this);
        pm.registerEvents(new onPlayerMove(),this);
        pm.registerEvents(new onPlayerDeath(),this);
        pm.registerEvents(new onFallDamage(),this);
        pm.registerEvents(new onPlayerJoin(),this);

        //Registering Commands
        this.getCommand("start").setExecutor(new start());
        this.getCommand("join").setExecutor(new join());
        this.getCommand("creategame").setExecutor(new creategame());
        this.getCommand("stopgame").setExecutor(new stopgame());

        //Registering Tasks
        BukkitScheduler scheduler = getServer().getScheduler();
        scheduler.scheduleSyncRepeatingTask(this,new everyTick(),0L,1L);
    }

    @Override
    public void onDisable() {
        Bukkit.getServer().unloadWorld("giantc_Production",false);
        worlds.deleteWorld("giantc_Production");
        //TODO: Disable plugin
    }



}
