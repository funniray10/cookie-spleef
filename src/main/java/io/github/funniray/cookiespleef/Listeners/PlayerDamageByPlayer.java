package io.github.funniray.cookiespleef.Listeners;

import io.github.funniray.cookiespleef.Classes.*;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftArrow;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.HashMap;

/**
 * Created by funniray on 6/22/17.
 */
public class PlayerDamageByPlayer implements Listener {
    @EventHandler
    public void onPlayerDamageByPlayer(EntityDamageByEntityEvent event){
        if (!(event.getEntity() instanceof CraftPlayer))
            return;
        if (!Games.isPlayerInGame((Player) event.getEntity()))
            return;
        if (event.getDamager() instanceof CraftArrow) {
            HashMap<Arrow, Cookie> cookies = Cookies.getCookies();
            Arrow arrow = (Arrow) event.getDamager();
            if (cookies.get(arrow) == null)
                return;
            Cookie cookie = cookies.get(arrow);
            cookie.getCookie().remove();
            arrow.remove();
            Cookies.remove(arrow);
            event.setDamage(2);
            Games.getPlayersGame((Player) event.getEntity()).getPlayerData((Player) event.getEntity()).setLastDamager((Player) ((CraftArrow) event.getDamager()).getShooter());
        }
        if (event.getDamager() instanceof CraftPlayer)
            Games.getPlayersGame((Player) event.getEntity()).getPlayerData((Player) event.getEntity()).setLastDamager((Player) event.getDamager());
        double damage = event.getDamage();
        double pHealth = ((Player) event.getEntity()).getHealth();
        if (pHealth - damage <= 0) {
            Player player = (Player) event.getEntity();
            event.setCancelled(true);
            player.setHealth(20);
            onDeath((Player) event.getEntity());
        }
    }

    public static void onDeath(Player player){
        Player killer;
        Game game = Games.getPlayersGame(player);
        InGame playerData = game.getPlayerData(player);
        if (playerData.getLastDamager() != player){
            killer = playerData.getLastDamager();
            InGame PlayerData = game.getPlayerData(killer);
            PlayerData.setKills(PlayerData.getKills()+1);
            game.setPlayerData(PlayerData);
            for (Player lplayer:game.getPlayers()) {
                lplayer.sendMessage(player.getDisplayName()+" Was killed by "+killer.getDisplayName());
            }
        }else{
            for (Player lplayer:game.getPlayers()) {
                lplayer.sendMessage(player.getDisplayName()+" Was killed by the void");
            }
        }
        if (!Games.isPlayerInGame(player))
            return;
        int offset = game.getID()*1000;
        player.setGameMode(GameMode.SPECTATOR);
        game.setPlayerData(game.getPlayerData(player).setLiving(false));
        player.teleport(new Location(player.getWorld(),0+offset,66,0+offset));
        game.checkLiving();
    }
}
