package io.github.funniray.cookiespleef.Listeners;

import io.github.funniray.cookiespleef.Classes.Game;
import io.github.funniray.cookiespleef.Classes.Games;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import static io.github.funniray.cookiespleef.Listeners.PlayerDamageByPlayer.onDeath;

/**
 * Created by funniray on 11/23/17.
 */
public class onFallDamage implements Listener {
    @EventHandler
    public void onFallDamage(EntityDamageEvent event){
        if (event.getEntity() instanceof CraftPlayer){
            Player player = (Player) event.getEntity();
            Game game = Games.getPlayersGame(player);
            if (game == null) {
                event.setCancelled(true);
            }else if (game.getState() != 1){
                event.setCancelled(true);
            }else if (game.getState() == 1){
                double damage = event.getDamage();
                double pHealth = player.getHealth();
                if (pHealth - damage <= 0) {
                    event.setCancelled(true);
                    player.setHealth(20);
                    onDeath(player);
                }
            }
        }
    }
}
