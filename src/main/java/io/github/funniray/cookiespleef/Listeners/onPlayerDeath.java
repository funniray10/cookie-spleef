package io.github.funniray.cookiespleef.Listeners;

import io.github.funniray.cookiespleef.Classes.Game;
import io.github.funniray.cookiespleef.Classes.Games;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * Created by funniray on 11/23/17.
 */
public class onPlayerDeath implements Listener {
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event){
        Player player = event.getEntity();
        Game game = Games.getPlayersGame(player);
        if (game != null){
            event.setDeathMessage(null);
            PlayerDamageByPlayer.onDeath(player);
        }else{
            player.teleport(new Location(player.getWorld(),8,4.1,8));
        }
    }
}
