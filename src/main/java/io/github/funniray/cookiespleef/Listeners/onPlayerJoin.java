package io.github.funniray.cookiespleef.Listeners;

import io.github.funniray.cookiespleef.Utils.worlds;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by funniray on 6/18/17.
 */
public class onPlayerJoin implements Listener {
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        worlds.teleportToWorld(event.getPlayer(),"world",8,4.1,8);
        event.getPlayer().setGameMode(GameMode.ADVENTURE);
    }
}
