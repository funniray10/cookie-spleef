package io.github.funniray.cookiespleef.Listeners;

import io.github.funniray.cookiespleef.Classes.Game;
import io.github.funniray.cookiespleef.Classes.Games;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

/**
 * Created by funniray on 11/23/17.
 */
public class onPlayerMove implements Listener {
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event){
        if (event.getTo().getY() <=0){
            Player player = event.getPlayer();
            Game game = Games.getPlayersGame(player);
            if (game != null){
                PlayerDamageByPlayer.onDeath(player);
            }else{
                player.teleport(new Location(player.getWorld(),8,4.1,8));
                player.setVelocity(new Vector(0,0,0));
            }
        }
    }
}
