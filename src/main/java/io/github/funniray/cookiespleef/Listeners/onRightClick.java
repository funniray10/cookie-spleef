package io.github.funniray.cookiespleef.Listeners;

import io.github.funniray.cookiespleef.Classes.Cookie;
import io.github.funniray.cookiespleef.Classes.Games;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by funniray on 6/14/17.
 */
public class onRightClick implements Listener {
    @EventHandler
    public void onRightClick(PlayerInteractEvent event){
        if (!Games.isPlayerInGame(event.getPlayer()))
            return;
        if (Games.getPlayersGame(event.getPlayer()).getState()!=1)
            return;
        if (!Games.getPlayersGame(event.getPlayer()).getPlayerData(event.getPlayer()).isLiving())
            return;
        if (event.getItem() == null)
            return;

        if (event.getItem().getType().equals(Material.STONE_AXE)) {
            ItemStack cookie = new ItemStack(Material.COOKIE);
            Location loc = event.getPlayer().getEyeLocation();
            cookie.setAmount(1);
            Arrow cookieArrow = event.getPlayer().getWorld().spawn(event.getPlayer().getLocation(), Arrow.class);
            cookieArrow.setShooter(event.getPlayer());
            Item cookieItem = event.getPlayer().getWorld().dropItem(loc, cookie);
            cookieItem.setPickupDelay(1000);
            cookieArrow.setVelocity(event.getPlayer().getLocation().getDirection().multiply(1.5));
            cookieItem.setVelocity(event.getPlayer().getLocation().getDirection().multiply(1.5));
            cookieArrow.setSilent(true);
            new Cookie(event.getPlayer(), cookieItem, cookieArrow);
            for (Player p : Games.getPlayersGame(event.getPlayer()).getPlayers()) {
                PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(cookieArrow.getEntityId());
                ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
            }
            cookieArrow.teleport(loc);
        }
    }
}
