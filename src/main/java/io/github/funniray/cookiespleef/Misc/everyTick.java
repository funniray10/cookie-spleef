package io.github.funniray.cookiespleef.Misc;

import io.github.funniray.cookiespleef.Classes.Cookie;
import io.github.funniray.cookiespleef.Classes.Cookies;
import net.minecraft.server.v1_12_R1.EntityArrow;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.util.BlockIterator;

import java.util.HashMap;

/**
 * Created by funniray on 6/18/17.
 */
public class everyTick implements Runnable {
    @Override
    public void run(){
        HashMap<Arrow,Cookie> cookies = Cookies.getCookies(); //List of cookies
        HashMap<Arrow,Cookie> toRemove = new HashMap<>(); //List of cookies to remove
        for (Arrow arrow : cookies.keySet()){
            EntityArrow entityArrow = ((CraftArrow)arrow).getHandle(); //Gets Minecraft arrow entity, not Bukkit's
            Cookie cookie = cookies.get(arrow); //Get Cookie class connected to arrow
            if (!entityArrow.inGround) { //Checks if arrow is in a block
                cookie.getCookie().teleport(arrow.getLocation()); //If it isn't, then tp the cookie to arrow
                cookie.getCookie().setVelocity(arrow.getVelocity());
                continue;
            }
            World world = arrow.getWorld(); //else, get arrow's wold
            BlockIterator bi = new BlockIterator(world, arrow.getLocation().toVector(), arrow.getVelocity().normalize(), 0, 4); //I have no clue honestly
            Block hit = null;

            while(bi.hasNext())
            {
                hit = bi.next();
                if(hit.getType()!= Material.AIR) //Grass/etc should be added probably since arrows doesn't collide with them
                    break;
            }
            //cookies.get(arrow).getPlayer().sendMessage(hit.getType().name());
            hit.setType(Material.AIR); //Remove block
            arrow.remove(); //Delete Arrow
            cookie.getCookie().remove(); //Delete Cookie
            toRemove.put(arrow,cookie); //Set to remove Cookie from list of Cookies
        }
        for (HashMap.Entry<Arrow, Cookie> entry : toRemove.entrySet()) {
            Cookies.remove(entry.getKey()); //Remove Cookie from list of Cookies
        }
    }
}
