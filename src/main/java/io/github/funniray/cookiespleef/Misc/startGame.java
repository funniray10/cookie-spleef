package io.github.funniray.cookiespleef.Misc;

import io.github.funniray.cookiespleef.Classes.Game;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;

import io.github.funniray.cookiespleef.Utils.worlds;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;


/**
 * Created by funniray on 6/18/17.
 */
public class startGame {
    public static void start(Game game){
        game.setState(1);
        int offset = game.getID()*1000;

        List<Player> players = game.getPlayers();
        for (Player player:players) {
            worlds.teleportToWorld(player,game.getWorld(),1+offset,66,1+offset);
            player.setGameMode(GameMode.SURVIVAL);
            player.getInventory().clear();
            player.getInventory().addItem(new ItemStack(Material.STONE_AXE));
            player.setHealth(20);
            player.setFoodLevel(20);
        }
    }
}
