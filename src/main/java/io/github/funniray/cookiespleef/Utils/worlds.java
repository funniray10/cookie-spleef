package io.github.funniray.cookiespleef.Utils;

import com.boydti.fawe.object.schematic.Schematic;
import com.boydti.fawe.util.EditSessionBuilder;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.extent.clipboard.BlockArrayClipboard;
import com.sk89q.worldedit.regions.*;
import org.bukkit.*;
import org.bukkit.entity.Player;

import java.io.*;

/**
 * Created by funniray on 6/21/17.
 */
public class worlds {

    public static boolean deleteWorld(World world){
        File file = world.getWorldFolder();
        try {
            org.apache.commons.io.FileUtils.deleteDirectory(file);
            return true;
        } catch (IOException e) {
            Bukkit.getServer().getLogger().warning(e.getMessage());
            return false;
        }
    }

    public static boolean deleteWorld(String world){
        File file = new File("./"+world);
        try {
            org.apache.commons.io.FileUtils.deleteDirectory(file);
            return true;
        } catch (IOException e) {
            Bukkit.getServer().getLogger().warning(e.getMessage());
            return false;
        }
    }

    public static void copyWorld(World from, World to,int offset){
        EditSession copyWorld = new EditSessionBuilder(from.getName()).autoQueue(false).build(); // See https://github.com/boy0001/FastAsyncWorldedit/wiki/WorldEdit-EditSession
        EditSession pasteWorld = new EditSessionBuilder(to.getName()).build(); // See https://github.com/boy0001/FastAsyncWorldedit/wiki/WorldEdit-EditSession
        Vector pos1 = new Vector(-16, 44, -16);
        Vector pos2 = new Vector(16, 68, 16);
        CuboidRegion copyRegion = new CuboidRegion(pos1, pos2);

        BlockArrayClipboard lazyCopy = copyWorld.lazyCopy(copyRegion);

        Schematic schem = new Schematic(lazyCopy);
        Vector too = new Vector(-16+offset, 44, -16+offset );
        schem.paste(pasteWorld, too, false);
        pasteWorld.flushQueue();
    }


    public static void teleportToWorld (Player player, String world, double x, double y, double z){
        World worldW = player.getServer().getWorld(world);
        if (worldW == null) {
            worldW = loadWorld(world);
        }
        player.teleport(new Location(worldW,x,y,z));
    }

    public static void teleportToWorld (Player player, World world, double x, double y, double z){
        player.teleport(new Location(world,x,y,z));
    }

    public static World loadWorld(String world){
        WorldCreator worldCreator = new WorldCreator(world);
        worldCreator.type(WorldType.FLAT);
        worldCreator.generator(new EmptyGenerator());
        return Bukkit.getServer().createWorld(worldCreator);
    }

    public static World loadMap(String world, int ID){
        World wWorld = loadWorld(world);
        WorldCreator w = new WorldCreator(world+"_Production");
        w.copy(wWorld);
        World nWorld = w.createWorld();
        nWorld.setAutoSave(false);
        copyWorld(wWorld,nWorld,ID*1000);
        return nWorld;
    }
}
